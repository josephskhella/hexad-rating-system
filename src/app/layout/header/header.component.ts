import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { UpdateMovie } from 'src/app/store/actions/movie.actions';
import { IAppState } from 'src/app/store/state/app.state';
import { Store } from '@ngrx/store';

@Component({
  selector: 'hexad-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  @Output() toggleMenu: EventEmitter<any> = new EventEmitter<any>();
  randomRatingFlag;
  fullscreen;

  constructor(
    private _store: Store<IAppState>
  ) { }


  ngOnInit() {
  }


  fireToggleMenu() {
    this.toggleMenu.emit();
  }

  toggleRandomRating() {
    this.randomRatingFlag = !this.randomRatingFlag;
    let randomRating = Math.floor(Math.random() * 5) + 1;
    let randomId = Math.floor(Math.random() * 13) + 1;
    this.randomeRate(randomId, randomRating);
  }

  randomeRate(id, rating) {
    if (this.randomRatingFlag) {
      let randomeTimeFrame = Math.floor(Math.random() * 5 * 1000) + 1;
      setTimeout(() => {
        console.log({ id, rating });
        this._store.dispatch(new UpdateMovie({ id, rating }));
        let randomRating = Math.floor(Math.random() * 5) + 1;
        let randomId = Math.floor(Math.random() * 13) + 1;
        this.randomeRate(randomId, randomRating);
      }, randomeTimeFrame);
    }
  }

  toggleFullScreen() {
    if (this.fullscreen) {
      if (document.exitFullscreen) {
        document.exitFullscreen();
      } else if (document['mozCancelFullScreen']) {
        /* Firefox */
        document['mozCancelFullScreen']();
      } else if (document['webkitExitFullscreen']) {
        /* Chrome, Safari and Opera */
        document['webkitExitFullscreen']();
      } else if (document['msExitFullscreen']) {
        /* IE/Edge */
        document['msExitFullscreen']();
      }
    } else {
      let elem = document.documentElement;
      let methodToBeInvoked = elem.requestFullscreen ||
        elem['webkitRequestFullScreen'] || elem['mozRequestFullscreen']
        ||
        elem['msRequestFullscreen'];
      if (methodToBeInvoked) methodToBeInvoked.call(elem);
    }
    this.fullscreen = !this.fullscreen;
  }

}
