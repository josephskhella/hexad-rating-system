import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '../../environments/environment';
import { IMovie } from '../models/movie.model';

@Injectable()
export class MovieService {
  moviesUrl = `${environment.apiUrl}/movies.json`;

  constructor(private _http: HttpClient) { }

  getMovies(): Observable<IMovie[]> {
    return this._http.get(this.moviesUrl)
      .pipe(map(data => <IMovie[]>data));
  }
}
