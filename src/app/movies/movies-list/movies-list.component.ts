import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { IAppState } from 'src/app/store/state/app.state';
import { GetMovies, UpdateMovie } from 'src/app/store/actions/movie.actions';
import { selectMovieList } from 'src/app/store/selectors/movie.selectors';

@Component({
  selector: 'hexad-movies-list',
  templateUrl: './movies-list.component.html',
  styleUrls: ['./movies-list.component.css'],
})
export class MoviesListComponent implements OnInit {
  movies$ = this._store.pipe(select(selectMovieList));

  constructor(
    private _store: Store<IAppState>
  ) { }

  ngOnInit() {
    this._store.dispatch(new GetMovies());
  }

  handleRatingClicked(rating, id) {
    this._store.dispatch(new UpdateMovie({id, rating}));
  }

}
