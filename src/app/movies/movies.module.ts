import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MoviesListComponent } from './movies-list/movies-list.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [MoviesListComponent],
  imports: [
    CommonModule,
    SharedModule
  ]
})
export class MoviesModule { }
