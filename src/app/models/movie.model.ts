export interface IMovie {
    id: number;
    author: string;
    name: string;
    image: string;
    rating: number;
}