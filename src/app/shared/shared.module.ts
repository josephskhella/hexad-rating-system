import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardComponent } from './components/card/card.component';
import { SafeHtmlPipe } from './pipes/safe-html.pipe';
import { RatingComponent } from './components/rating/rating.component';
import { OrderByPipe } from './pipes/order-by.pipe';

@NgModule({
  declarations: [CardComponent, RatingComponent, SafeHtmlPipe, RatingComponent, OrderByPipe],
  imports: [
    CommonModule
  ],
  exports: [CardComponent, RatingComponent, SafeHtmlPipe, RatingComponent, OrderByPipe]
})
export class SharedModule { }
