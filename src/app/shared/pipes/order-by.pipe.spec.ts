import { OrderByPipe } from './order-by.pipe';

describe('OrderByPipe', () => {
  it('should order', () => {
    const pipe = new OrderByPipe();
    let orderedArr = pipe.transform([{id: 2, rating: 2}, {id: 3, rating: 3}, {id: 1, rating: 1}], 'rating');
    console.log(orderedArr);
    expect(orderedArr[0]['id']).toBe(3);
    expect(orderedArr[1]['id']).toBe(2);
    expect(orderedArr[2]['id']).toBe(1);
  });
});
