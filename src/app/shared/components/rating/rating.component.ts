import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'hexad-rating',
  templateUrl: './rating.component.html',
  styleUrls: ['./rating.component.css']
})
export class RatingComponent implements OnInit {
  @Output() ratingClicked: EventEmitter<number> = new EventEmitter<number>();
  @Input() rating;
  ratingArr = [5, 4, 3, 2, 1];
  constructor() { }

  ngOnInit() {
  }

  rate(rating) {
    this.ratingClicked.emit(rating);
  }

}
