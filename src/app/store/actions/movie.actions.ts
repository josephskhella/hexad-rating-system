import { Action } from '@ngrx/store';
import { IMovie } from 'src/app/models/movie.model';

export enum EMovieActions {
    GetMovies = '[Movie] Get Movies',
    GetMoviesSuccess = '[Movie] Get Movies Success',
    GetMovie = '[Movie] Get Movie',
    GetMovieSuccess = '[Movie] Get Movie Success',
    UpdateMovie = '[Movie] Update Movie',
    UpdateMovieSuccess = '[Movie] Update Movie Success'
}

export class GetMovies implements Action {
    public readonly type = EMovieActions.GetMovies;
}
export class GetMoviesSuccess implements Action {
    public readonly type = EMovieActions.GetMoviesSuccess;
    constructor(public payload: IMovie[]) {}
}
export class GetMovie implements Action {
    public readonly type = EMovieActions.GetMovie;
    constructor(public payload: number) {}
}
export class GetMovieSuccess implements Action {
    public readonly type = EMovieActions.GetMovieSuccess;
    constructor(public payload: IMovie) {}
}
export class UpdateMovie implements Action {
    public readonly type = EMovieActions.UpdateMovie;
    constructor(public payload: any) {}
}
export class UpdateMovieSuccess implements Action {
    public readonly type = EMovieActions.UpdateMovieSuccess;
    constructor(public payload: any) {}
}

export type MovieActions = GetMovies | GetMoviesSuccess | GetMovie | GetMovieSuccess | UpdateMovie | UpdateMovieSuccess;