import { Injectable } from "@angular/core";
import { Effect, Actions, ofType } from "@ngrx/effects";
import { IAppState } from '../state/app.state';
import { Store, select } from '@ngrx/store';
import { map, withLatestFrom, switchMap, mergeMap, mapTo } from 'rxjs/operators';
import { of, pipe } from 'rxjs';
import { GetMovie, EMovieActions, GetMovieSuccess, GetMovies, GetMoviesSuccess, UpdateMovie, UpdateMovieSuccess } from '../actions/movie.actions';
import { selectMovieList } from '../selectors/movie.selectors';
import { IMovie } from 'src/app/models/movie.model';
import { MovieService } from 'src/app/services/movie.service';

@Injectable()
export class MoviesEffects {
    @Effect()
    getMovie$ = this._actions$.pipe(
        ofType<GetMovie>(EMovieActions.GetMovie),
        map(action => action.payload),
        withLatestFrom(this._store.pipe(select(selectMovieList))),
        switchMap(([id, movies]) => {
            const selectedMovie = movies.filter(movie => movie.id === id)[0];
            return of(new GetMovieSuccess(selectedMovie));
        })
    );

    @Effect()
    getMovies$ = this._actions$.pipe(
        ofType<GetMovies>(EMovieActions.GetMovies),
        switchMap(() => this._MovieService.getMovies()),
        switchMap((movies: IMovie[]) => of(new GetMoviesSuccess(movies)))
    );

    @Effect()
    updateMovie$ = this._actions$.pipe(
        ofType<UpdateMovie>(EMovieActions.UpdateMovie),
        withLatestFrom(this._store.pipe(select(selectMovieList))),
        switchMap((idAndRating) => {return of(new UpdateMovieSuccess(idAndRating[0]))})
    );

    constructor(
        private _MovieService: MovieService,
        private _actions$: Actions,
        private _store: Store<IAppState>
    ) { }
}