import { IMovie } from 'src/app/models/movie.model';

export interface IMovieState {
    movies: IMovie[];
    selectedMovie: IMovie;
}

export const initialMovieState: IMovieState = {
    movies: null,
    selectedMovie: null
}