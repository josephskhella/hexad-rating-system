import { RouterReducerState } from '@ngrx/router-store';
import { initialMovieState, IMovieState } from './movie.state';

export interface IAppState {
    router?: RouterReducerState;
    movies: IMovieState;
}

export const initialAppState: IAppState = {
    movies: initialMovieState,
};

export function getInitialState(): IAppState {
    return initialAppState;
}