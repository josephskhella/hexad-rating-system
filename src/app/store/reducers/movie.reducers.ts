import { initialMovieState, IMovieState } from '../state/movie.state';
import { MovieActions, EMovieActions } from '../actions/movie.actions';

export const movieReducers = (
    state = initialMovieState,
    action: MovieActions
): IMovieState => {
    switch (action.type) {
        case EMovieActions.GetMoviesSuccess: {
            return {
                ...state,
                movies: action.payload
            }
        }
        case EMovieActions.GetMovieSuccess: {
            return {
                ...state,
                selectedMovie: action.payload
            }
        }
        case EMovieActions.UpdateMovieSuccess: {
            return {
                ...state,
                movies: state.movies.map((movie) => {
                    if (movie.id === action.payload.payload.id) {
                        movie.rating = action.payload.payload.rating;
                    }
                    return movie;
                })
            }
        }

        default:
            return state;
    }
}