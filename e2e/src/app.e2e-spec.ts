import { AppPage } from './app.po';
import { browser, logging, element, by } from 'protractor';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should have title', () => {
    page.navigateTo();
    browser.ignoreSynchronization = true;
    expect(element(by.css('.welcome-msg')).getText()).toContain('Hexad Rating System');
  });

  it('should not random rate', () => {
    page.navigateTo();
    browser.ignoreSynchronization = true;
    browser.sleep(1000);
    expect(element(by.css('.card__title')).getText()).toContain(element(by.css('.card__title')).getText());
  });

  it('should random rate', async () => {
    page.navigateTo();
    browser.ignoreSynchronization = true;
    browser.sleep(1000);
    let prevElements = element.all(by.css('.card__title'));
    let prevElementsText = [];
    prevElements.each((element, index) => {
      element.getText().then((text) => {
        prevElementsText.push(text);
      });
    });
    let toggleBtn = element(by.css('.mat-slide-toggle'));
    toggleBtn.click();
    browser.sleep(10000);
    let curElements = element.all(by.css('.card__title'));
    let sameName = true;
    let curElementsText = [];
    curElements.each((element, index) => {
      element.getText().then((text) => {
        curElementsText.push(text);
      });
    });
    toggleBtn.click();
    browser.sleep(1000);
    curElements.each((element, index) => {
      if (prevElementsText[index] !== curElementsText[index]) {
        sameName = false;
      }
    }).then(() => {
      expect(sameName).toEqual(false);
    });
  });



  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
